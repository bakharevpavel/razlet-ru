(function() {
    'use strict';

    angular.module('app').config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/app');
        $stateProvider.state('app', {
            url: '/app',
            views: {
                '': {
                    templateUrl: "templates/main.html",
                    controller: 'AppController'
                },
                'table@app': {
                    templateUrl: "templates/table.html",
                    controller: 'TableController'
                },
                'graph@app': {
                    templateUrl: "templates/graph.html",
                    controller: 'GraphController'
                }
            }

        });
    }]);

})();