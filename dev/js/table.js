(function() {
    'use strict';

	angular.module('app').controller('TableController', ['$scope', function($scope) {
        $scope.delete = function(id) {
            $scope.$parent.db.del('coords', {
                id: id
            }).then(function() {
            	$scope.$parent.read();
            });
        };
	}]);

})();