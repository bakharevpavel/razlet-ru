(function() {
    'use strict';

    angular.module('app', [
        'ui.router',
        'highcharts-ng',
        'angular-websql'
    ]).controller('AppController', ['$scope', '$webSql', function($scope, $webSql) {
        $scope.items = [];

        $scope.db = $webSql.openDatabase('db', '1.0', 'Test DB', 2 * 1024 * 1024);

        $scope.db.createTable('coords', {
            "id": {
                "type": "INTEGER",
                "null": "NOT NULL",
                "primary": true,
                "auto_increment": true
            },
            "x": {
                "type": "INTEGER",
                "null": "NOT NULL",
            },
            "y": {
                "type": "INTEGER",
                "null": "NOT NULL",
            }
        });

        $scope.coords = {
            x: 0,
            y: 0
        };

        $scope.insert = function(x, y) {
            $scope.db.insert('coords', {
                x: x,
                y: y
            }).then(function(results) {
                $scope.read();
            });
        };

        $scope.read = function() {
            var arr = [];
            $scope.db.selectAll('coords').then(function(results) {
                for(var i = 0; i < results.rows.length; i++){
                    arr.push(results.rows.item(i));

                };

                $scope.items = arr;
            });
        };

        $scope.read();
    }]);
})();