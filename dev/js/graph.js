(function() {
    'use strict';

	angular.module('app').controller('GraphController', ['$scope', function($scope) {
        $scope.chartConfig = {
            options: {
                plotOptions: {
                    line:{
                        lineWidth: 3
                    }
                },
                scrollbar: {
                    enabled: false
                },
                navigator: {
                    enabled: true
                }
            },
            chart: {
                backgroundColor: 'transparent'
            },

            colors: ['#c65f10'],

            exporting: {
                enabled: false
            },

            navigation:{
                menuItemHoverStyle: {
                    background: '#c65f10',
                    color: '#000'
                },
                menuItemStyle: {
                    padding: '3px 10px',
                    color: '#000',
                    fontSize: '0.6rem'
                },
                menuStyle: {
                    background: '#fff'
                }
            },

            navigator: {
                maskFill: 'rgba(255, 255, 255, 0.15)',
                series: {
                    color: '#c65f10'
                }
            },

            useHighStocks: true,

            series: [{
                name: '',
                data: [],
                tooltip: {
                    valueDecimals: 2,
                    yDecimals: 0,
                    xDateFormat: '%H:%M %e %B %Y г.',
                    pointFormat: '<b>{point.y}</b> {series.name}'
                },
                threshold : null,
                fillColor : {
                    linearGradient : {
                        x1: 0,
                        y1: 0,
                        x2: 0,
                        y2: 1
                    },
                    stops : [
                        [0, '#c65f10'],
                        [1, Highcharts.Color('#c65f10').setOpacity(0).get('rgba')]
                    ]
                }
            }],

            xAxis: {
                lineColor: '#000',
                labels: {
                    style: {
                        color: '#000',
                        fontSize: '0.7rem'
                    },
                    format: '{value:%d.%m}'
                },
                tickColor: '#c65f10',
                tickLength: 10,
                tickWidth: 3,
                tickPosition: 'inside'
            },

            yAxis: {
                gridLineColor: '#000',
                gridLineWidth: 0.5,
                labels: {
                    align: 'center',
                    style: {
                        color: '#000',
                        fontSize: '0.7rem'
                    }
                }
            }
        };


		$scope.$parent.$watch('items', function(val) {
            var x = [], y = [];

            for(var key in val) {
            	x.push(val[key].x);
            	y.push(val[key].y)
            }
                
            $scope.chartConfig.series[0].name = x;
            $scope.chartConfig.series[0].data = y;
		});
	}]);

})();