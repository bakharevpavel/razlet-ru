# Test task for Razlet.ru

## Installation

```sh

	$ npm install
	$ bower install
	$ gulp

```

## Usage

```sh

	$ yo electron-humble
	$ npm start

```

Then access http://localhost:8080 .