const gulp = require('gulp');
const stylus = require('gulp-stylus');
const pug = require('gulp-pug');
const autoprefixer = require('gulp-autoprefixer');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const preen = require('preen');

var paths = {
    pugMain: [
    	'!./dev/templates/**/*.pug',
        './dev/pug/**/*.pug'
    ],
    pugTemplates: [
    	'./dev/templates/**/*.pug'
    ],
    stylus: [
        './dev/stylus/**/*.styl'
    ],
    js: [
        './dev/js/**/*.js'
    ]
};

function handleError(error) {
    console.log(error.toString());
}

gulp.task('default', ['lib', 'js', 'stylus', 'pug', 'templates', 'watch']);

gulp.task('js', () => {
    return gulp.src(paths.js)
        .pipe(concat("app.js"))
        .pipe(gulp.dest("./app/js/"))
        .pipe(rename('app.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest("./app/js/"))
});

gulp.task('lib', (done) => {
    preen.preen({}, () => {
        gulp.src('')
    });
});

gulp.task('stylus', () => {
    return gulp.src(paths.stylus)
        .pipe(stylus({
            compress: true,
        }))
        .pipe(concat('styles.css'))
        .pipe(autoprefixer({
            browsers: ['last 4 versions'],
            cascade: false
        }))
        .pipe(gulp.dest('./app/css/'))
        .on('error', handleError);
});

gulp.task('pug', () => {
    return  gulp.src(paths.pugMain)
        .pipe(pug())
        .pipe(gulp.dest('./app/'))
        .on('error', handleError);
});

gulp.task('templates', () => {
    return  gulp.src(paths.pugTemplates)
        .pipe(pug())
        .pipe(gulp.dest('./app/templates/'))
        .on('error', handleError);
});

gulp.task('watch', () => {
    gulp.watch(paths.js, ['js']);
    gulp.watch(paths.stylus, ['stylus']);
    gulp.watch(paths.pugMain, ['pug']);
    gulp.watch(paths.pugTemplates, ['templates']);
});